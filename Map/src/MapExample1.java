import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapExample1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("A map is a Data structure using Key, Value Pairs");
		Map<String,String>map1=new HashMap<String,String>();
		Map<String,String>map2=new TreeMap<String,String>();
		map1.put("School", "A palce where you can go to learn");
		map1.put("Sun", "The center of the solar System");
		map1.put("Jupitor", "The largest planet");
		map1.put("Mercury", "The smallest planet");
		map1.put("Moon", "Rotate around the earth");
		map1.put("Moon", "A natural satellite");
		System.out.println(map1.keySet());
		//enhanced for loops
		for(String word:map1.keySet()) {
			System.out.println(word+" "+map1.get(word));
		}
		System.out.println(map1);
	}

}
/*A map is a Data structure using Key, Value Pairs
[School, Moon, Jupitor, Sun, Mercury]
School A palce where you can go to learn
Moon A natural satellite
Jupitor The largest planet
Sun The center of the solar System
Mercury The smallest planet
{School=A palce where you can go to learn, Moon=A natural satellite, Jupitor=The largest planet, Sun=The center of the solar System, Mercury=The smallest planet}
*/
