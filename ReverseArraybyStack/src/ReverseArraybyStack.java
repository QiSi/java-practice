import java.util.Stack;

public class ReverseArraybyStack {

	public static void reverseArray(int[] array) {
		Stack<Integer> stack = new Stack<Integer>();
		int i;
		for(i = 0;i<array.length;i++) {
			stack.push(array[i]);
		}
		for(i = 0;i<array.length;i++) {
			array[i]=stack.pop();
		}
		return;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = {1,2,3,4,5};
		System.out.println("Array Before Reverse");
		for(int i=0;i<array.length;i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println(" ");
		
		reverseArray(array);
		System.out.println("Array After Reverse");
		for(int i=0;i<array.length;i++) {
			System.out.print(array[i]+" ");
		}
		
	}

}
/* 
 * Array Before Reverse
 * 1 2 3 4 5  
   Array After Reverse
   5 4 3 2 1 
 * 
*/