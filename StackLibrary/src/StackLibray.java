import java.util.*;

public class StackLibray {
	public static int getPriority(String token) {
		if (token.equals("+") || token.equals("-")) {
			return 3;
		} else if (token.equals("*") || token.equals("/")) {
			return 2;
		} else if (token.equals("^")) {
			return 1;
		} else {
			return (Integer) null;
		}
	}

	public static boolean isLeftParenthesis(String token) {
		if (token.equals("(")) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isRightParenthesis(String token) {
		if (token.equals(")")) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isOperator(String token) {
		if (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/") || token.equals("^")) {
			return true;
		} else {
			return false;
		}
	}

	public static String infixToPostfix(String inFix) {
		String postFix = "";
		StringTokenizer tokenizer = new StringTokenizer(inFix); // tokenizer of the infix expression
		Stack<String> stack = new Stack<String>();
		String tokenInFix = ""; // used for the token from the infix
		String tokenStack = ""; // used for the string or token taken for the stack
		while (tokenizer.hasMoreTokens()) {
			tokenInFix = tokenizer.nextToken();
			if (isOperator(tokenInFix)) {
				if (stack.isEmpty() || isLeftParenthesis(stack.peek())) {
					stack.push(tokenInFix);
				} else {
					while (!stack.isEmpty() && isOperator(stack.peek())
							&& getPriority(tokenInFix) >= getPriority(stack.peek())) {
						tokenStack = stack.pop() + " ";
						postFix += tokenStack + " ";
					} // End while
					stack.push(tokenInFix);
				} // End else
			} // End IfOperator
			else if (isLeftParenthesis(tokenInFix)) {
				// push it into the stack
				stack.push(tokenInFix);
			}

			else if (isRightParenthesis(tokenInFix)) {
				// pop off all of thr operators
				// add to the postfix
				// until we get to the lest Parenthesis
				// pop the left Parenthesis off the stack & discard it

				while (!isLeftParenthesis(stack.peek())) {
					postFix += stack.pop() + " ";
				} // end while
				stack.pop();
			} else {
				// appear operand to postFix
				postFix += tokenInFix + " ";
			}
		} // End big while
		while (!stack.isEmpty()) {
			postFix += stack.pop() + " ";
		}
		return postFix;
	}// end method

	public static int evaluatePostFixExpression(String PF) {
		int ans = 0;
		Stack<String> stack = new Stack<String>();
		StringTokenizer tokenizer = new StringTokenizer(PF);
		String token;
		while (tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken();
			if (isOperator(token)) {
				stack.push(evaluateBinaryExpression(stack.pop(), token, stack.pop()));
			} else {
				stack.push(token);
			}
		} // End while

		String answer = stack.pop();
		ans = Integer.parseInt(answer);
		return ans;
	}

	public static String evaluateBinaryExpression(String op1, String oper, String op2) {
		int answer = 0;
		int numOp2 = Integer.parseInt(op1);
		int numOp1 = Integer.parseInt(op2);
		// change op1,op2 to a number
		// numOp1, numOp2

		if (oper.equals("*")) {
			answer = numOp1 * numOp2;
		}
		if (oper.equals("+")) {
			answer = numOp1 + numOp2;
		}
		if (oper.equals("-")) {
			answer = numOp1 - numOp2;
		}
		if (oper.equals("/")) {
			answer = numOp1 / numOp2;
		}
		if (oper.equals("^")) {
			answer = (int) Math.pow(numOp1, numOp2);
		}
		String ans = answer + "";
		return ans;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inFix = "";
		int answer = 0;

		System.out.println("\n*** The first part is just evaluating the post fix worth 10 points ***");
		String postFix = "8 4 + 10 6 - /";
		answer = evaluatePostFixExpression(postFix);
		System.out.println("The postFix expression is ");
		System.out.println(postFix);
		System.out.println("The answer is " + answer + " it is worth 10 points");

		System.out.println("\n***This part is worth 4 points for a total of 14 ");
		inFix = "( 32 + 16 / 4 * 2 )";
		postFix = infixToPostfix(inFix);
		System.out.println(inFix);
		System.out.println(postFix);
		answer = evaluatePostFixExpression(postFix);
		System.out.println("The answer is " + answer);

		System.out.println("\n***This part is worth 2 points for a total of 16 ");
		inFix = "8 + 4 * 2 ^ 3 - 9";
		postFix = infixToPostfix(inFix);
		System.out.println(inFix);
		System.out.println(postFix);
		answer = evaluatePostFixExpression(postFix);
		System.out.println("The answer is " + answer);

		System.out.println("\n***This part is worth 4 points for a total of 20 ");
		inFix = "( 8 + 4 * 2 ^ 3 - 5 ) / ( 3 - 2 * 5 + 3 ^ 2 + 4 )";
		postFix = infixToPostfix(inFix);
		System.out.println(inFix);
		System.out.println(postFix);
		answer = evaluatePostFixExpression(postFix);
		System.out.println("The answer is " + answer);

		System.out.println("End main");

	}// end main

}// end class

/*
 *** The first part is just evaluating the post fix worth 10 points *** The
 * postFix expression is 8 4 + 10 6 - / The answer is 3 it is worth 10 points
 *** 
 * This part is worth 4 points for a total of 14 ( 32 + 16 / 4 * 2 ) 32 16 4 / 2
 * * + The answer is 40
 *** 
 * This part is worth 2 points for a total of 16 8 + 4 * 2 ^ 3 - 9 8 4 2 3 ^ * +
 * 9 - The answer is 31
 *** 
 * This part is worth 4 points for a total of 20 ( 8 + 4 * 2 ^ 3 - 5 ) / ( 3 - 2
 * * 5 + 3 ^ 2 + 4 ) 8 4 2 3 ^ * + 5 - 3 2 5 * - 3 2 ^ + 4 + / The answer is 5
 * End main
 */
